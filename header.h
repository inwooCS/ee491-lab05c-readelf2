// Source for the header file https://github.com/file/file/blob/master/src/readelf.h
// man elf.h
// readelf.h

#include <stdint.h>

#define EI_NIDENT   16

typedef uint32_t	Elf32_Addr;
typedef uint32_t	Elf32_Off;
typedef uint16_t	Elf32_Half;
typedef uint32_t	Elf32_Word;
typedef uint8_t		Elf32_Char;

typedef	uint64_t 	Elf64_Addr;
typedef	uint64_t 	Elf64_Off;
typedef uint64_t 	Elf64_Xword;
typedef uint16_t	Elf64_Half;
typedef uint32_t	Elf64_Word;
typedef uint8_t		Elf64_Char;

typedef struct {
    Elf32_Char	e_ident[EI_NIDENT];
    Elf32_Half	e_type;
    Elf32_Half	e_machine;
    Elf32_Word	e_version;
    Elf32_Addr	e_entry;  /* Entry point */
    Elf32_Off	e_phoff;
    Elf32_Off	e_shoff;
    Elf32_Word	e_flags;
    Elf32_Half	e_ehsize;
    Elf32_Half	e_phentsize;
    Elf32_Half	e_phnum;
    Elf32_Half	e_shentsize;
    Elf32_Half	e_shnum;
    Elf32_Half	e_shstrndx;
} Elf32_Ehdr;

typedef struct {
    Elf64_Char	e_ident[EI_NIDENT];
    Elf64_Half	e_type;
    Elf64_Half	e_machine;
    Elf64_Word	e_version;
    Elf64_Addr	e_entry;  /* Entry point */
    Elf64_Off	e_phoff;
    Elf64_Off	e_shoff;
    Elf64_Word	e_flags;
    Elf64_Half	e_ehsize;
    Elf64_Half	e_phentsize;
    Elf64_Half	e_phnum;
    Elf64_Half	e_shentsize;
    Elf64_Half	e_shnum;
    Elf64_Half	e_shstrndx;
} Elf64_Ehdr;


typedef struct elf32_shdr {
    Elf32_Word  sh_name;
    Elf32_Word  sh_type;
    Elf32_Word  sh_flags;
    Elf32_Addr  sh_addr;
    Elf32_Off   sh_offset;
    Elf32_Word  sh_size;
    Elf32_Word  sh_link;
    Elf32_Word  sh_info;
    Elf32_Word  sh_addralign;
    Elf32_Word  sh_entsize;
} Elf32_Shdr;

typedef struct elf64_shdr{
    Elf64_Word  sh_name;
    Elf64_Word  sh_type;
    Elf64_Off   sh_flags;
    Elf64_Addr  sh_addr;
    Elf64_Off   sh_offset;
    Elf64_Off   sh_size;
    Elf64_Word  sh_link;
    Elf64_Word  sh_info;
    Elf64_Off   sh_addralign;
    Elf64_Off   sh_entsize;
} Elf64_Shdr;


#define SHT_NULL    0       /* Section header table entry unused */
#define SHT_PROGBITS    1       /* Program specific (private) data */
#define SHT_SYMTAB  2       /* Link editing symbol table */
#define SHT_STRTAB  3       /* A string table */
#define SHT_RELA    4       /* Relocation entries with addends */
#define SHT_HASH    5       /* A symbol hash table */
#define SHT_DYNAMIC 6       /* Information for dynamic linking */
#define SHT_NOTE    7       /* Information that marks file */
#define SHT_NOBITS  8       /* Section occupies no space in file */
#define SHT_REL     9       /* Relocation entries, no addends */
#define SHT_SHLIB   10      /* Reserved, unspecified semantics */
#define SHT_DYNSYM  11      /* Dynamic linking symbol table */
#define SHT_INIT_ARRAY    14        /* Array of ptrs to init functions */
#define SHT_FINI_ARRAY    15        /* Array of ptrs to finish functions */
#define SHT_PREINIT_ARRAY 16        /* Array of ptrs to pre-init funcs */
#define SHT_GROUP     17        /* Section contains a section group */
#define SHT_SYMTAB_SHNDX  18        /* Indicies for SHN_XINDEX entries */
#define SHT_LOOS    0x60000000  /* First of OS specific semantics */
#define SHT_HIOS    0x6fffffff  /* Last of OS specific semantics */
#define SHT_GNU_INCREMENTAL_INPUTS 0x6fff4700   /* incremental build data */
#define SHT_GNU_ATTRIBUTES 0x6ffffff5   /* Object attributes */
#define SHT_GNU_HASH    0x6ffffff6  /* GNU style symbol hash table */
#define SHT_GNU_LIBLIST 0x6ffffff7  /* List of prelink dependencies */
/* The next three section types are defined by Solaris, and are named
   SHT_SUNW*.  We use them in GNU code, so we also define SHT_GNU*
   versions.  */
#define SHT_SUNW_verdef 0x6ffffffd  /* Versions defined by file */
#define SHT_SUNW_verneed 0x6ffffffe /* Versions needed by file */
//#define SHT_GNU_verdef  SHT_SUNW_verdef
//#define SHT_GNU_verneed SHT_SUNW_verneed
//#define SHT_GNU_versym  SHT_SUNW_versym
#define SHT_LOPROC  0x70000000  /* Processor-specific semantics, lo */
#define SHT_HIPROC  0x7FFFFFFF  /* Processor-specific semantics, hi */
#define SHT_LOUSER  0x80000000  /* Application-specific semantics */
/* #define SHT_HIUSER   0x8FFFFFFF    *//* Application-specific semantics */
#define SHT_HIUSER  0xFFFFFFFF  /* New value, defined in Oct 4, 1999 Draft */
