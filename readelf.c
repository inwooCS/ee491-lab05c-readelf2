///
/// @author In Woo Park <inwoo@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   2/24/2021
/// @ver 	2.0
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "header.h"
#include <sys/mman.h> 


#define bufferSize 1024
typedef enum {F, T} boolean;

/* Initialize struct headers */
Elf32_Ehdr Header32;
Elf64_Ehdr Header64; 

Elf32_Shdr SHeader32;
Elf64_Shdr SHeader64;

void full (unsigned char buffer[]) {

	for(int i = 0; i < 16; i ++ ){
		printf("%02x ", buffer[i]);
	}
	printf("\n");

	for(int i = 16; i < 32; i ++ ){
		printf("%02x ", buffer[i]);
	}
	printf("\n");

	for(int i = 32; i < 48; i ++ ){
		printf("%02x ", buffer[i]);
	}
	printf("\n");
	
	for(int i = 48; i < 64; i ++ ){
		printf("%02x ", buffer[i]);
	}
	printf("\n");
}

/*
 * This function processes the Magic Header from readelf.
 * It will read the buffer and return the Magic number or the 
 * first row when you call readelf. Those magic numbers have properties 
 * such as 7c 45 4c 46 which spells out _ E L F, or the file type ELF. 
 */ 
void magic (unsigned char buffer[]) {

	/* 
	 * Initialize Variables for error check.
	 * These integers are the integer values of buffer[0...3]. 
	 */
	
	int i;
	unsigned char S = 127;
	unsigned char E = 69;
	unsigned char L = 76; 
	unsigned char F = 70;

	/* Error check to see if the file is an ELF file,
	 * we want to stop immediately if not.
	 */
	if (buffer[0] != S && buffer[1] != E 
			&& buffer[2] != L && buffer[3] != F){
		printf("File is not an ELF file.\n");
		exit(EXIT_FAILURE);
	}

	/* Everything below is the contents for ELF Header we want to see */
	printf("ELF Header:\n");
	printf("  Magic:  ");
	for (i = 0; i < 16; i++){
		printf("%02x ", buffer[i]);
	}
	printf("\n");
}

/*
 * This function processes the Class Header from readelf.
 * It reads the buffer and checks the 4th index to determine 
 * if it is 32 or 64 architecture. 
 */
void class (unsigned char buffer[]) {

	unsigned char architecture64 = 2;
	unsigned char architecture32 = 1;

	if (buffer[4] == architecture64) {
		printf("  Class:				");
		printf("ELF64\n");
		//Address_Size = 1;
	}

	if (buffer[4] == architecture32) {
		printf("  Class:				");
		printf("ELF32\n");
		//Address_Size = 0;
	}
}

/* 
 * Read buffer[5] get info.
 */
void data (unsigned char buffer[]) {

	unsigned char leastSigBit = 1;
	unsigned char mostSigBit = 2;

	if (buffer[5] == leastSigBit) {
		printf("  Data:					");
		printf("%d's commplement, little endian\n", mostSigBit);
	}

	if (buffer[5] == mostSigBit) {
		printf("  Data:					");
		printf("%d's commplement, big endian\n", leastSigBit);
	}
}

/*
 * If buffer[6] is 1, return version 1.
 */
void version (unsigned char buffer[]) {

	//%d, grab version from elf file. 
	unsigned char version = buffer[6]; 

	printf("  Version:				");
	printf("%d (current)\n", version);
}

void OS_ABI (unsigned char buffer[]) {

	const unsigned char ELFOSABI_NONE       = 0   ;    /* UNIX System V ABI */
	const unsigned char ELFOSABI_HPUX       = 1   ;    /* HP-UX */

	if (buffer[7] == ELFOSABI_NONE) {
		printf("  OS/ABI:				");
		printf("UNIX - System V\n");
	}

	if (buffer[7] == ELFOSABI_HPUX) {
		printf("  OS/ABI:				");
		printf("UNIX - HP_UX\n");
	}
}

void ABIVersion (unsigned char buffer[]) {

	unsigned char ABIversion = 0; 

	if (buffer[8] == ABIversion) {
		printf("  ABI Version:				");
		printf("0\n");
	}
}

void type (unsigned char buffer[]) {

	unsigned char REL = 1; 
	unsigned char EXEC = 2; 
	unsigned char DYN = 3; 
	unsigned char CORE = 4; 

	//int x = buffer[17];
	//printf("%d", x);

	if (buffer[16] == REL){
		printf("  Type:					");
		printf("REL (Relocatable file)\n");
	}

	if (buffer[16] == EXEC){
		printf("  Type:					");
		printf("EXEC (Relocatable file)\n");
	}

	if (buffer[16] == DYN){
		printf("  Type:					");
		printf("DYN (Relocatable file)\n");
	}

	if (buffer[16] == CORE){
		printf("  Type:					");
		printf("CORE (Relocatable file)\n");
	}
}

/* 
 * Only 3 machines are implemented because there are too many. 
 */
void machine (unsigned char buffer[]) {

	unsigned char EM_NONE = 0;
	unsigned char EM_M386 = 3;
	unsigned char EM_X86_64 = 62;

	printf("  Machine:				");

	if (buffer[18] == EM_X86_64) {
		printf("Advanced Micro Devices x86-64\n");
	} else if (buffer[18] == EM_M386) {
		printf("Intel 80386\n");
	} else { 
		printf("%c", EM_NONE);
	}
}

void version2 (unsigned char buffer[]) {

	unsigned char version2 = buffer[20];

	printf("  Version:				");
	printf("0x%d \n", version2);
}

/*
 * This function returns the architecture type. 
 */
int findType (unsigned char buffer[]) {
	unsigned char architecture64 = 2;
	unsigned char architecture32 = 1;

	int type64 = 64;
	int type32 = 32;

	if (buffer[4] == architecture64) {
		
		return type64;
	}

	if (buffer[4] == architecture32) {
		
		return type32;
	}

	return 0;
}

/* Starting from this function, I will just call the struct I made */
void entryPointAddress (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Entry point address:			");
		printf("0x %x \n", Header32.e_entry);
	}

	if ((type == 64)) {

		printf("  Entry point address:			");
		printf("0x%lx \n", Header64.e_entry);
		
	}
}


void startOfProgramHeaders (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Start of program headers:		");
		printf("%d (bytes into file) \n", Header32.e_phoff);
	}

	if ((type == 64)) {

		printf("  Start of program headers:		");
		printf("%02ld (bytes into file) \n", Header64.e_phoff);
	}
}

void startOfSectionHeaders (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Start of section headers:		");
		printf("%d (bytes into file) \n", Header32.e_shoff);
	}

	if ((type == 64)) {

		printf("  Start of section headers:		");
		printf("%ld (bytes into file) \n", Header64.e_shoff);
	}
}

void flags (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Flags:		");
		printf("%d \n", Header32.e_flags);
	}

	if ((type == 64)) {

		printf("  Flags:				");
		printf("0x%d \n", Header64.e_flags);
	}
}

void sizeOfThisHeader (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Size of this headers:			");
		printf("%d \n (bytes)", Header32.e_ehsize);
	}

	if ((type == 64)) {

		printf("  Size of this headers:			");
		printf("%d (bytes)\n", Header64.e_ehsize);
	}
}

void sizeOfProgramHeaders (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Size of this program headers:		");
		printf("%d \n (bytes)", Header32.e_phentsize);
	}

	if ((type == 64)) {

		printf("  Size of this program headers:		");
		printf("%d (bytes)\n", Header64.e_phentsize);
	}
}

void numberOfProgramHeaders (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Number of program headers:		");
		printf("%d \n ", Header32.e_phnum);
	}

	if ((type == 64)) {

		printf("  Number of program headers:		");
		printf("%d \n", Header64.e_phnum);
	}
}

void sizeOfSectionHeaders (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Size of section headers:		");
		printf("%d \n (bytes)", Header32.e_shentsize);
	}

	if ((type == 64)) {

		printf("  Size of section headers:		");
		printf("%d (bytes)\n", Header64.e_shentsize);
	}
}

void numberOfSectionHeaders (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf("  Number of section headers:		");
		printf("%d \n ", Header32.e_shnum);
	}

	if ((type == 64)) {

		printf("  Number of section headers:		");
		printf("%d \n", Header64.e_shnum);
	}
}

void sectionHeaderStringTableIndex (unsigned char buffer[]) {

	int type = 0;
	type = findType(buffer);

	if ((type == 32)) {

		printf(" Section header string table index:		");
		printf("%d \n ", Header32.e_shstrndx);
	}

	if ((type == 64)) {

		printf("  Section header string table index:	");
		printf("%d \n", Header64.e_shstrndx);
	}
}


void printSHFormat() {
	printf(" [Nr]   Name\n");
	printf("  	Type 		Address 	Offset 		Link\n");
	printf("  	Size 		EntSize  	Info  		Align\n");
	printf("  	Flags\n");
}

void sh_name (unsigned char buffer[], char arrSectionNames[256][256], int j) {

	if (findType(buffer) == 32){
		printf(" [%d]  ", j);
		printf(" %s ", arrSectionNames[j]);	
	}

	if (findType(buffer) == 64){
		printf(" [%d]  ", j);
		printf("  %s ", arrSectionNames[j]);	
	}

	printf("\n");
	
}

void sh_type (int type) {
	
	switch(type) {
		case SHT_NULL:
			printf("	NULL 		");
		break;
		case SHT_PROGBITS:
			printf("	PROGBITS 	");
		break;
		case SHT_SYMTAB:
			printf("	SYMTAB 		");
		break;
		case SHT_STRTAB:
			printf("	STRTAB 		");
		break;
		case SHT_RELA:
			printf("	RELA 		");
		break;
		case SHT_HASH:
			printf("	HASH 		");
		break;
		case SHT_DYNAMIC:
			printf("	DYNAMIC 	");
		break;
		case SHT_NOTE:
			printf("	NOTE 		");
		break;
		case SHT_NOBITS:
			printf("	NOBITS 		");
		break;
		case SHT_REL:
			printf("	REL 		");
		break;
		case SHT_SHLIB:
			printf("	SHLIB 		");
		break;
		case SHT_DYNSYM:
			printf("	DYNSYM 		");
		break;
		case SHT_INIT_ARRAY:
			printf("	INIT_ARRAY 	");
		break;
		case SHT_FINI_ARRAY:
			printf("	FINI_ARRAY 	");
		break;
		case SHT_PREINIT_ARRAY:
			printf("	PREINIT_ARRAY ");
		break;
		case SHT_GROUP:
			printf("	GROUP 		");
		break;
		case SHT_SYMTAB_SHNDX:
			printf("	SYMTAB_SHNDX ");
		break;
		case SHT_LOOS:
			printf("	LOOS  		");
		break;
		case SHT_HIOS:
			printf("	HIOS  		");
		break;
		case SHT_GNU_INCREMENTAL_INPUTS:
			printf("	GNU INCREMENTAL_INPUTS 	");
		break;
		case SHT_GNU_ATTRIBUTES:
			printf("	GNU ATTRIBUTES 	");
		break;
		case SHT_GNU_HASH:
			printf("	GNU HASH 	");
		break;
		case SHT_GNU_LIBLIST:
			printf("	GNU LILIST 		");
		break;
		case SHT_SUNW_verdef:
			printf("	SUNW VERDEF 	");
		break;
		case SHT_SUNW_verneed:
			printf("	SUNW VERNEED 	");
		break;
		case SHT_LOPROC:
			printf("	LOPROC 			");
		break;
		case SHT_HIPROC:
			printf("	HIPROC 			");
		break;
		case SHT_LOUSER:
			printf("	LOUSER  		");
		break;
		case SHT_HIUSER:
			printf("	HIUSER 			");
		break;
	}

}

void zeros (uint64_t y) {

 
	int zero = 0;

	for (int i = 0; i < sizeof(y)+ 3; i++) {
		printf("%d", zero);
	}

}

void sh_address (uint64_t address) {
	
	zeros(address);
	printf("%lx 	", address);
}

void sh_offset (uint64_t offset) {

	zeros(offset);
	printf("%lx 	", offset);
}

void sh_link (uint64_t link) {

	printf("%lx", link);
}

void sh_size (uint64_t size) {

	printf("	");
	zeros(size +3);
	printf("%lx 	", size);
}

void sh_entSize (uint16_t entSize) {

	//printf("	");
	zeros(entSize);
	printf("%x 	", entSize);
	
}

void sh_info (uint32_t info) {

	printf("%x		", info);
	
}

void sh_align (uint64_t align) {

	printf("%lx 	", align);
	
}


void printZeros(int x, uint64_t flags) {

	int zeros = 16 - x; 
	int zero = 0;

	printf("	[");

	for (int i = 0; i < zeros; i++) {
		printf("%d", zero);
	}
	printf("%lx]:", flags);
}

void sh_flags (uint64_t flags) {

	switch(flags){
		case 0:
			printZeros(1, flags);
			printf(" ");
		break;
		case SHF_ALLOC:
			printZeros(1, flags);
			printf("ALLOC");
		break;
		case 3:
			printZeros(1, flags);
			printf("WRITE, ALLOC");
		break;
		case 6:
			printZeros(1, flags);
			printf("ALLOC, EXEC");
		break;
		case 30:
			printZeros(2, flags);
			printf("MERGE, STRINGS");
		break;
		case ALLOC_INFO_LINK:
			printZeros(2, flags);
			printf("ALLOC, INFO, LINK");
		break;
	}
	
}


/*
 * This is the main function that will accept for now >./readelf -h [filename]
 * Just like the original readelf, it's purpose is to return (currently) headers. 
 */
int main (int argc, char*argv[]) {

	/* Error check for not enough arguments in argc. "usage" */
	if (argc < 3) {
		fprintf(stderr, "usage: ./readelf [command i.e. -h] [FILE...]\n");
		exit(EXIT_FAILURE);
	}
 
 	/* Initialize variables (boolean, opt, count) */
	boolean do_header = F;
	boolean do_sectionHeader = F;
	int opt;
	//Address_Size = ADDRESS_SIZE_UNKNOWN;
	//int count = argc;

	/* 
	 * Loop that uses getopt for command line arguments.
	 * The idea is to use a switch statement to grab additioal 
	 * commands line arguments (i.e. "-h"). 
	 */ 
	while ((opt = getopt(argc, argv, "h:t")) != -1) {
		switch(opt) {
			case 'h':;
				/* what the heck is happening here idk */
				//i want to process this as a header file 
				do_header = T;
				break;
			case 't':
				do_sectionHeader = T;
				break;
			case 'x':

			case '?':
				printf("unknown option: %c\n", optopt);
				break;
		}	
	}

	/* Open the file and create a buffer[] of predefined size */
	FILE * file = fopen(argv[argc - 1], "r");
	unsigned char buffer[bufferSize];

	/* Error check to see if the file opens succesfully */
	if (file == NULL) {
		perror("");
		exit(EXIT_FAILURE);
	}

	/* Read the file and store it into the buffer nicely */ 
	size_t newLen = fread(buffer, sizeof(char), bufferSize, file);
	if (ferror(file) != 0) {
		fputs("Error reading the file", stderr);
		exit(EXIT_FAILURE);
	} else {
		buffer[newLen++] = '\0';
	}
	
	/* Because I'm going to fread more than once, I will rewind the file so it goes back
	 * to the top. I will also read it into a struct I made in a file called header.h. 
	 */
	rewind(file);
	fread(&Header32, sizeof(Header32), 1, file);
	rewind(file);
	fread(&Header64, sizeof(Header64), 1, file);

	rewind(file);

	char arrSectionNames[256][256];

	fclose(file);

	/*
	 * Because the switch statement triggered a boolean 
	 * We are going to start calling the header functions 
	 * to return the contents we want, passing in the buffer 
	 * which is what they are going to read. 
	 */
	if (do_header == T) {
		magic(buffer);
		class(buffer);
		data(buffer);
		version(buffer);
		OS_ABI(buffer);
		ABIVersion(buffer);
		type(buffer);
		machine(buffer);
		version2(buffer);
		entryPointAddress(buffer);
		startOfProgramHeaders(buffer);
		startOfSectionHeaders(buffer);
		flags(buffer);
		sizeOfThisHeader(buffer);
		sizeOfProgramHeaders(buffer);
		numberOfProgramHeaders(buffer);
		sizeOfSectionHeaders(buffer);
		numberOfSectionHeaders(buffer);
		sectionHeaderStringTableIndex(buffer);

	}

	if (do_sectionHeader == T) {
		
		FILE * file = fopen(argv[argc - 1], "r");
		unsigned char buffer[bufferSize];
		size_t newLen = fread(buffer, sizeof(char), bufferSize, file);

		if (ferror(file) != 0) {
			fputs("Error reading the file", stderr);
			exit(EXIT_FAILURE);
		} else {
			buffer[newLen++] = '\0';
		}		

		if (findType(buffer) == 32) {

			/* Make an array of struct section headers with provided offset and size */
			Elf32_Shdr segments32[Header32.e_shnum]; //make an array of struct section headers 
			fseek(file, Header32.e_shoff, SEEK_SET); //starting point 
			fread(segments32, Header32.e_shentsize, Header32.e_shnum, file); //read from starting point, size 

			/* Grab names from segments */
			char * namesAsString = malloc(segments32[Header32.e_shstrndx].sh_size); //make a string whatever the size is 
			fseek(file, segments32[Header32.e_shstrndx].sh_offset, SEEK_SET);
			fread(namesAsString, segments32[Header32.e_shstrndx].sh_size, 1, file);

			/* store the names into an array */
			for (int i = 0; i < Header32.e_shnum; i++) {
				strcpy(arrSectionNames[i], namesAsString + segments32[i].sh_name);
			}

			printSHFormat();

			for(int j = 0; j < Header64.e_shnum; j ++) {

				sh_name (buffer, arrSectionNames, j);

				uint32_t types= segments32[j].sh_type;
				sh_type (types);

				uint32_t addresses = segments32[j].sh_addr;
				sh_address(addresses);
				//printf("	");

				uint32_t offset = segments32[j].sh_offset;
				sh_offset(offset);

				uint32_t link = segments32[j].sh_link;
				sh_link(link);

				printf("\n");

				uint32_t size = segments32[j].sh_size;
				sh_size(size);

				uint16_t entSize = segments32[j].sh_entsize;
				sh_entSize(entSize);

				uint32_t info = segments32[j].sh_info;
				sh_info(info);

				uint32_t align = segments32[j].sh_addralign;
				sh_align(align);

				printf("\n");

				uint32_t flags = segments32[j].sh_flags;
				sh_flags(flags);

				printf("\n");
			}

		}
	
		if (findType(buffer) == 64) {

			/* Make an array of struct section headers with provided offset and size */
			Elf64_Shdr segments64[Header64.e_shnum];
			 //make an array of struct section headers 
			fseek(file, Header64.e_shoff, SEEK_SET); //starting point 
			fread(segments64, Header64.e_shentsize, Header64.e_shnum, file); //read from starting point, size 
		
			/* Grab names from segments */
			char * namesAsString = malloc(segments64[Header64.e_shstrndx].sh_size); //make a string whatever the size is 
			fseek(file, segments64[Header64.e_shstrndx].sh_offset, SEEK_SET);
			fread(namesAsString, segments64[Header64.e_shstrndx].sh_size, 1, file);


			/* store the names into an array */
			for (int i = 0; i < Header64.e_shnum; i++) {
				strcpy(arrSectionNames[i], namesAsString + segments64[i].sh_name);
			}

			printSHFormat();

			for(int j = 0; j < Header64.e_shnum; j ++) {

				sh_name (buffer, arrSectionNames, j);

				int types = segments64[j].sh_type;
				sh_type (types);

				uint64_t addresses = segments64[j].sh_addr;
				sh_address(addresses);
				//printf("	");

				uint64_t offset = segments64[j].sh_offset;
				sh_offset(offset);

				uint64_t link = segments64[j].sh_link;
				sh_link(link);

				printf("\n");

				uint64_t size = segments64[j].sh_size;
				sh_size(size);

				uint16_t entSize = segments64[j].sh_entsize;
				sh_entSize(entSize);

				uint32_t info = segments64[j].sh_info;
				sh_info(info);

				uint64_t align = segments64[j].sh_addralign;
				sh_align(align);

				printf("\n");

				uint64_t flags = segments64[j].sh_flags;
				sh_flags(flags);

				printf("\n");
			}
				//uint64_t flag11= segments64[0].sh_flags; 
				//printf("%lu", flag11);
		}
		fclose(file);

	}
}
